<?php

namespace App\Http\Controllers;

use App\ModelTodo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $data = ModelTodo::all();
        return response($data);
    }

    public function show($id)
    {
        $data = ModelTodo::where('id', $id)->get();
        return response ($data);
    }
    
    public function store(Request $request)
    {
        $data = new ModelTodo();
        $data->activity = $request->input('activity');
        $data->description = $request->input('description');
        $data->save();

        return response('Berhasil tambah data');
    }

    public function update(Request $request, $id)
    {
        //pakai xx-www-form-urlencode
        $data = ModelTodo::where('id', $id)->first();
        $data->activity = $request->input('activity');
        $data->description = $request->input('description');
        $data->save();

        return response('berhasil update data');
    }

    public function destroy($id)
    {
        $data = ModelTodo::where('id', $id)->first();
        $data->delete();

        return response('berhasil delete data');
    }

    //
}
